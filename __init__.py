# -*- coding: utf-8 -*-

def classFactory(iface):
   
    from myfirstplugin.my_plugin import MyPlugin
    return MyPlugin(iface)
